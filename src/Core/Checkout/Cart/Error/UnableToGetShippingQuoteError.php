<?php declare(strict_types=1);

namespace SmcShipcloudLive\Core\Checkout\Cart\Error;

use Shopware\Core\Checkout\Cart\Error\Error;

class UnableToGetShippingQuoteError extends Error
{
	private $key = "unable-to-get-shipping-quote";
	private $text;

	public function __construct(string $message)
	{
		$this->key = 'unable-to-get-shipping-quote';
		$this->text = $message;
	}

	public function getParameters(): array
	{
		return ['key' => $this->key, 'text' => $this->text];
	}

	public function getId(): string
	{
		return $this->key;
	}

	public function getMessageKey(): string
	{
		return $this->key;
	}

	public function getLevel(): int
	{
		return self::LEVEL_ERROR;
	}

	public function blockOrder(): bool
	{
		return true;
	}
}
