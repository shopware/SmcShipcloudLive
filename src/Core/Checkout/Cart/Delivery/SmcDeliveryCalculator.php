<?php declare(strict_types=1);

namespace SmcShipcloudLive\Core\Checkout\Cart\Delivery;

use Shopware\Core\Checkout\Cart\Cart;
use Shopware\Core\Checkout\Cart\Delivery\Struct\Delivery;
use Shopware\Core\Checkout\Cart\Delivery\Struct\DeliveryCollection;
use Shopware\Core\Checkout\Cart\Delivery\DeliveryCalculator;
use Shopware\Core\Checkout\Cart\LineItem\CartDataCollection;
use Shopware\Core\Checkout\Cart\LineItem\LineItemCollection;
use Shopware\Core\Checkout\Cart\Price\QuantityPriceCalculator;
use Shopware\Core\Checkout\Cart\Price\Struct\CalculatedPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\CartPrice;
use Shopware\Core\Checkout\Cart\Price\Struct\QuantityPriceDefinition;
use Shopware\Core\Checkout\Cart\Tax\PercentageTaxRuleBuilder;
use Shopware\Core\Checkout\Shipping\Aggregate\ShippingMethodPrice\ShippingMethodPriceCollection;
use Shopware\Core\Checkout\Shipping\Aggregate\ShippingMethodPrice\ShippingMethodPriceEntity;
use Shopware\Core\Checkout\Shipping\Cart\Error\ShippingMethodBlockedError;
use Shopware\Core\Checkout\Shipping\Exception\ShippingMethodNotFoundException;
use Shopware\Core\Checkout\Shipping\ShippingMethodEntity;
use Shopware\Core\Defaults;
use Shopware\Core\Framework\DataAbstractionLayer\Pricing\Price;
use Shopware\Core\Framework\DataAbstractionLayer\Pricing\PriceCollection;
use Shopware\Core\Framework\Util\FloatComparator;
use Shopware\Core\System\SalesChannel\SalesChannelContext;

class SmcDeliveryCalculator implements DeliveryCalculator
{

	private $priceCalculator;
	private $percentageTaxRuleBuilder;

	public function __construct(
		QuantityPriceCalculator $priceCalculator,
		PercentageTaxRuleBuilder $percentageTaxRuleBuilder
	) {
		$this->priceCalculator = $priceCalculator;
		$this->percentageTaxRuleBuilder = $percentageTaxRuleBuilder;
	}

	public function calculate(CartDataCollection $data, Cart $cart, DeliveryCollection $deliveries, SalesChannelContext $context): void
	{
		foreach ($deliveries as $delivery) {
			//$this->calculateDelivery($data, $cart, $delivery, $context);
			$costs = $this->calculateShippingCosts($delivery->getShippingMethod(),
								new PriceCollection([new Price(Defaults::CURRENCY, 23, 0, false)]),
								$delivery->getPositions()->getLineItems(),
								$context);
			$delivery->setShippingCosts($costs);
		}
	}
}
